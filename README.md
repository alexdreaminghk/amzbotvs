# AmzBotVS

This is the cron job used to regularly check price in different website, store the price and send telegram message to me when there is any change in price. Currently only support amazon. Item to be checked is stored in mongodb. When it is completed, it should accept url, xpath of price in that page and the corresponding user to notify in telegram so that it support multi user and multi website.

Example "See Generic Price Tracker.PNG"

## Getting started
Create a my-net network.
Create a mongodb in my-net network.
Update all the config in docker-compose.yml and "docker-compose up -d"