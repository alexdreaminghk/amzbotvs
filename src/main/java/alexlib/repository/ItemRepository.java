package alexlib.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import alexlib.entity.Item;

public interface ItemRepository extends MongoRepository<Item, String> {
    public List<Item> findByUrl(String url);
}

