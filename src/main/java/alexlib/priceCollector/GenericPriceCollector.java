package alexlib.priceCollector;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Date;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import alexlib.entity.Item;
import alexlib.entity.Price;
import alexlib.priceCollector.siteStrategy.WebsiteStrategy;

public class GenericPriceCollector {

    public static final int MAX_RESURSIVE_302 = 10;
    private HttpClient httpClient;
    private String userAgent;
    private WebsiteStrategy websiteStrategy;
    private static final Logger logger = LoggerFactory.getLogger(GenericPriceCollector.class);

    public GenericPriceCollector(HttpClient httpClient, String userAgent, WebsiteStrategy websiteStrategy) {
        this.httpClient = httpClient;
        this.userAgent = userAgent;
        this.websiteStrategy = websiteStrategy;
    }

    public void getPrice(Item item) {
        try {
            HttpResponse<String> response = getHttpResponseForItem(item);

            Document document = Jsoup.parse(response.body(), "utf-8");

            BigDecimal priceVal;

            if (!websiteStrategy.isOutOfStock(document)) {
                // in stock
                String priceStr = websiteStrategy.getItemPriceStr(document);
                logger.info("Price: " + getPriceValue(priceStr));
                priceVal = getPriceValue(priceStr);
            } else {
                // out of stock
                priceVal = BigDecimal.valueOf(-1);
            }

            Price price = new Price(new Date(), priceVal);
            item.addPrice(price);
            item.setItemDesc(websiteStrategy.getItemDesc(document));

        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private HttpResponse<String> getHttpResponseForItem(Item item) throws IOException, InterruptedException {
        HttpRequest request = getHttpRequestFromUrl(item.getUrl(), Optional.empty());

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        //keep trying if 302
        if (response.statusCode()==302)
        {
            return recursiveHttpRequest(response, 0);
        }

        return response;
    }

    //Morrison would put you in a queue and return 302. Once you get 200, you will be redirected to the original path
    private HttpResponse<String> recursiveHttpRequest(HttpResponse<String> response, int depth) throws IOException, InterruptedException {

        logger.debug("Recursively accessing url=["+response.uri()+"], depth=["+depth+"]");

        logger.debug("Response header="+response.headers());
        logger.debug("Response body="+response.body().toString());
        //TODO: Not to use Thread.sleep
        //TODO: Use system parameter
        Thread.sleep(500);

        validateDepth(depth);

        Optional<String> responseLocationHeader= response.headers().firstValue("Location");


        //get cookie
        Optional<String> setCookieHeaderVal = getCookieValFromResponse(response);

        if (responseLocationHeader.isPresent()) {

            HttpRequest nestRequest = getHttpRequestFromUrl(responseLocationHeader.get(), setCookieHeaderVal);

            HttpResponse<String> nestResponse = httpClient.send(nestRequest, HttpResponse.BodyHandlers.ofString());



            //keep trying if 302
            if (nestResponse.statusCode()==302)
            {
                return recursiveHttpRequest(nestResponse, ++depth);
            }
            else
            {
                return nestResponse;
            }
        }
        else
        {
            throw new RuntimeException("302 Response without Location. Response=["+ response +"]");
        }
    }

    private static Optional<String> getCookieValFromResponse(HttpResponse<String> response) {
        Optional<String> setCookieHeaderVal = response.headers().firstValue("set-cookie");

        //Get the first part of cookie only
        if (setCookieHeaderVal.isPresent())
        {
            String cookieVal = setCookieHeaderVal.get().split(";")[0];
            setCookieHeaderVal = Optional.of(cookieVal);
        }
        return setCookieHeaderVal;
    }

    private static void validateDepth(int depth) {
        if (depth > MAX_RESURSIVE_302)
        {
            throw new RuntimeException("Recursively getting 302 exceeding the maximum defined depth. Max=["+MAX_RESURSIVE_302+"]");
        }
    }

    private HttpRequest getHttpRequestFromUrl(String responseLocationHeader, Optional<String> cookie) {

        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder().GET().uri(URI.create(responseLocationHeader))
                .setHeader("User-Agent", userAgent) // add request header
                //.setHeader("Accept-Encoding", "gzip, deflate, br")
                .setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                .setHeader("Accept-Language", "zh-HK,zh-Hant;q=0.9");

        if (cookie.isPresent()) {
            logger.debug("Request URL=["+responseLocationHeader+"]");
            logger.debug("Request cookie=["+cookie.get()+"]");
            httpRequestBuilder.setHeader("Cookie", cookie.get());
        }

        return httpRequestBuilder.build();
    }

    public static BigDecimal getPriceValue(String input) {
        String output = "";

        Pattern pattern = Pattern.compile("\\d{1,3}[,\\.]?(\\d{1,2})?");
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            output = matcher.group(0);
            output = output.replaceAll(",", "");
        }
        return new BigDecimal(output);
    }

    public HttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }
}
