package alexlib.priceCollector.siteStrategy;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import us.codecraft.xsoup.Xsoup;

import java.util.List;

public class MorrisonStrategy implements WebsiteStrategy {

    @Override
    public boolean isOutOfStock(Document document) {
        //TODO: not seen this scenario yet. cannot implement.
        return false;
    }

    @Override
    public String getItemDesc(Document document) {
        List<Element> elements = Xsoup.compile("//*[@id=\"overview\"]/section[1]/header/h1").evaluate(document).getElements();

        if (elements.size()>0)
            return elements.get(0).text();
        else
            throw new RuntimeException("Morrison specific ItemDesc Not Found in page");
    }

    ////*[@id="overview"]/section[2]/div[1]/div/h2/meta[1]
    @Override
    public String getItemPriceStr(Document document) {
        List<Element> priceElement = Xsoup.compile("//*[@id='overview']/section[2]/div[1]/div/h2/meta[1]").evaluate(document).getElements();

        if (priceElement.size()>0)
            return priceElement.get(0).getAllElements().attr("content");
        else
            throw new RuntimeException("Morrison specific itemPrice Not Found in page");
    }
}
