package alexlib.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Item {
    
    @Id
    private String id;
    private String itemDesc;

    @Indexed
    private String url;
    private List<Price> prices;

    public Item(String url) {
        this.url = url;
        prices = new ArrayList<Price>();
    }

    public String getItemDesc() {
        return itemDesc;
    }
    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public List<Price> getPrices() {
        return prices;
    }
    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public void addPrice(Price price){
        this.prices.add(price);
    }

    @Override
    public String toString() {
        return "Item [itemDesc=" + itemDesc + ", prices=" + prices + ", url=" + url + "]";
    }
    
    

}
