package alexlib.config;

import java.util.Collection;
import java.util.Collections;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.connection.ConnectionPoolSettings;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "alexlib")
public class MongoConfig extends AbstractMongoClientConfiguration{
    @Override
    protected String getDatabaseName() {
        return "amzbotvs";
    }
 
    @Override
    public MongoClient mongoClient() {

        String DB_HOSTNAME = System.getenv("db_hostname");
        String DB_USERNAME = System.getenv("db_username");
        String DB_PASSWORD = System.getenv("db_password");

        ConnectionString connectionString = new ConnectionString("mongodb://"+DB_USERNAME+":"+DB_PASSWORD+"@"+DB_HOSTNAME+":27017/amzbotvs?authSource=admin");
        
        ConnectionPoolSettings.Builder connectionPoolBuilder = ConnectionPoolSettings.builder()
            .maxSize(100);

        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
            .applyConnectionString(connectionString)
            .applyToConnectionPoolSettings(builder1 -> builder1.applySettings(connectionPoolBuilder.build()))
            .build();
        
        return MongoClients.create(mongoClientSettings);
    }

    @Override
    public Collection getMappingBasePackages() {
        return Collections.singleton("alexlib");
    }
 
}
